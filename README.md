## akita-user 15 AP4A.241205.013 12621605 release-keys
- Manufacturer: google
- Platform: zuma
- Codename: akita
- Brand: google
- Flavor: akita-user
- Release Version: 15
- Kernel Version: 5.15.153
- Id: AP4A.241205.013
- Incremental: 12621605
- Tags: release-keys
- CPU Abilist: arm64-v8a
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/akita/akita:15/AP4A.241205.013/12621605:user/release-keys
- OTA version: 
- Branch: akita-user-15-AP4A.241205.013-12621605-release-keys-3308
- Repo: google/akita
